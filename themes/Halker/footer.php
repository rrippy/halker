<div class="push"></div>

</div><!-- #wrapper -->

<footer id="footer" class="clearfix">

	<div id="footer_content">
	
    		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'footer-menu' ) ); ?>

            <div class="copyright">Copyright &copy; <?php the_time('Y') ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved.</div>
         		
        
   </div><!-- #footer_content -->
                    
   
</footer>

</div><!-- #wrapper_all -->


<?php wp_footer(); ?>

  </body>
</html>