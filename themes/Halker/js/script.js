$(document).ready(function(){
	
	
	// flex slider - map
	$('.map').flexslider({
		animation: "fade",
		controlNav: false,
		directionNav: false,
		slideshow: false,
		itemWidth: 366,
		initDelay: 50
	});
	
  // flex slider - home page
	$('.flexslider').flexslider({
		animation: "fade",
		controlNav: true,
		directionNav: false,
		slideshow: true
	});
	
	
	
	
	
	
  // mouseover gallery thumbnails
  $('.gallery_btn').hover(function() {
    $(this)
      .find('.gallery_hover')
	  .animate({top:56},180);
  // mouseout thumbnails
  }, function () {
	 $(this)
		.find('.gallery_hover')
		.animate({top:266},180);
  });
  
  
  
  // map hover fadein block
  $('.btn_1').mouseover(function(){
	  $(this).addClass('onButton');
	  if ($('.map_hover_info.denver-julesburg-basin').hasClass('inView')) {
	  } else {
      	$('.map_hover_info.denver-julesburg-basin').delay(30).fadeIn(300);
	  }
  });
  // fadeout block if not hoverring over it
  $('.btn_1').mouseout(function(){
	  $(this).removeClass('onButton');
	  setTimeout(function (){
		  if ($('.map_hover_info.denver-julesburg-basin').hasClass('inView')) {
		  } else {
			$('.map_hover_info.denver-julesburg-basin').fadeOut(250);
		  }
	  }, 30);
  });
  $('.btn_2').mouseover(function(){
	  if ($('.map_hover_info.san-juan-basin').hasClass('inView')) {
	  } else {
      	$('.map_hover_info.san-juan-basin').delay(30).fadeIn(300);
	  }
  });
  // fadeout block if not hoverring over it
  $('.btn_2').mouseout(function(){
	  setTimeout(function (){
		  if ($('.map_hover_info.san-juan-basin').hasClass('inView')) {
		  } else {
			$('.map_hover_info.san-juan-basin').delay(30).fadeOut(250);
		  }
	   }, 30);
  });
  $('.btn_3').mouseover(function(){
	  if ($('.map_hover_info.permain-basin').hasClass('inView')) {
	  } else {
      	$('.map_hover_info.permain-basin').delay(30).fadeIn(300);
	  }
  });
  // fadeout block if not hoverring over it
  $('.btn_3').mouseout(function(){
	 setTimeout(function (){
		  if ($('.map_hover_info..permain-basin').hasClass('inView')) {
		  } else {
			$('.map_hover_info..permain-basin').delay(30).fadeOut(250);
		  }
	  }, 30);
  });
  $('.btn_4').mouseover(function(){
	  if ($('.map_hover_info.dwilliston-basin').hasClass('inView')) {
	  } else {
      	$('.map_hover_info.williston-basin').delay(30).fadeIn(300);
	  }
  });
  // fadeout block if not hoverring over it
  $('.btn_4').mouseout(function(){
	  setTimeout(function (){
		  if ($('.map_hover_info.williston-basin').hasClass('inView')) {
		  } else {
			$('.map_hover_info.williston-basin').delay(30).fadeOut(250);
		  }
	  }, 30);
  });

  
  // show if you're hovering over a block
  $('.map_hover_info').hover(function() {
	  $(this).addClass('inView');
	  // mouseout
  }, function () {
	 $(this).removeClass('inView');
	 setTimeout(function (){
		 if ($('.map_location').hasClass('onButton')) {
		} else {
			$('.map_hover_info').delay(26).fadeOut(250);
		}
	}, 3);
  });  
	  


  
});