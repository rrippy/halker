<?php get_header(); the_post(); ?>

<?php
$intro_image = get_field('intro_image');
$intro_text = get_field('intro_text');
$page_link_blocks = get_field('page_link_blocks');
?>


<?php $child_pages = $wpdb->get_results("SELECT *    FROM $wpdb->posts WHERE post_parent = ".$post->ID."    AND post_type = 'page' ORDER BY menu_order", 'OBJECT');    ?>
	
    <div id="main" class="clearfix">
    
    
    	<nav class="service_nav sub-nav" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>
    	</nav><!-- .service_nav.sub-nav -->
        
        
        <div class="full_culumn">
        
        <?php if ($intro_image) { ?>
			<?php $lead_image = wp_get_attachment_image_src($intro_image, 'slide-image'); ?>
          		<div class="about_image"><img src="<?php echo $lead_image[0]; ?>" class="lead_img" /></div>
        <?php } ?>
        
        <?php if ($intro_text) { ?>
            <h2><?= $intro_text ?></h2>
        <?php } ?>
    

    
    
    
    <div class="section_buttons">
        
        <?php if($page_link_blocks): ?> 
        
        	<?php foreach($page_link_blocks as $page_link_block): ?>
	
    			<div class="section_block">
                
                
                  <a href="<?= $page_link_block['page_link'] ?>" class="block_btn">
                  
					  <?php if($page_link_block['block_title']): ?>
                            <h4><?=$page_link_block['block_title'] ?></h4>
                      <?php endif; ?>
                      
                      <?php $page_link = wp_get_attachment_image_src($page_link_block['block_image'], 'block-thumb'); ?>
                      <img src="<?php echo $page_link[0]; ?>" />
                      
                  </a>
                  
                  <?php if($page_link_block['intro_text']): ?>
                        <div class="block_intro"><?=$page_link_block['intro_text'] ?></div>
                  <?php endif; ?>
                  
                  <?php if($page_link_block['body_text']): ?>
                        <div class="block_body"><?=$page_link_block['body_text'] ?></div>
                  <?php endif; ?>
       
                  
               </div><!--.section_block--> 

            <?php endforeach; ?>
        
        <?php endif;?>
            
            
       </div><!--.section_buttons--> 

       
      </div><!--.full_culumn--> 
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
