<?php get_header(); the_post(); 
$slider_images = get_field('slider_images');
$page_link_blocks = get_field('page_link_blocks');


$recent_posts = get_posts(array(
	'post_type' => 'post',
	'numberposts' => 4,
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'post_date'
));

?>


<?php if($slider_images): ?>
        
<div class="flexslider homeslider">
  <ul class="slides">
    <?php foreach($slider_images as $slider_image): ?>
        <li>
          <?php $slide_image = wp_get_attachment_image_src($slider_image['slider_image'], 'full-slide-image'); ?>
          <img src="<?php echo $slide_image[0]; ?>" />
          <?php if($slider_image['image_caption']): ?>
                <div class="value_overlay"><div class="slider_controls">
                	<h3 class="flex-caption"><?=$slider_image['image_caption'] ?>
                     <?php if($slider_image['slider_link']): ?>
                    	<a href="<?=$slider_image['slider_link'] ?>" class="slider_link_btn">Learn more</a>
                     <?php endif; ?>
                     </h3>
                </div></div>
          <?php endif; ?>
        </li>
    <?php endforeach; ?>
  </ul><!--.slides--> 
</div><!--.flexslider--> 
    

<?php endif;?>

	
    <div id="main" class="clearfix">

 
    	    
    	<div class="intro_text">
        	<h2>Halker Consulting helps companies move faster, increase production and productivity, safely and efficiently.&nbsp;&nbsp;<a href="<?php echo esc_url( home_url( '/why-halker/' ) ); ?>">Learn more</a></h2> 
        </div>
        
        
        
        <div class="section_buttons">
        
        <?php if($page_link_blocks): ?> 
        
        	<?php foreach($page_link_blocks as $page_link_block): ?>
	
    			<div class="section_block">
                
                
                  <a href="<?= $page_link_block['page_link'] ?>" class="block_btn">
                  
					  <?php if($page_link_block['block_title']): ?>
                            <h4><?=$page_link_block['block_title'] ?></h4>
                      <?php endif; ?>
                      
                      <?php $page_link = wp_get_attachment_image_src($page_link_block['block_image'], 'block-thumb'); ?>
                      <img src="<?php echo $page_link[0]; ?>" />
                      
                  </a>
                  
                  <?php if($page_link_block['intro_text']): ?>
                        <div class="block_intro"><?=$page_link_block['intro_text'] ?></div>
                  <?php endif; ?>
                  
                  <?php if($page_link_block['body_text']): ?>
                        <div class="block_body"><?=$page_link_block['body_text'] ?></div>
                  <?php endif; ?>
       
                  
               </div><!--.section_block--> 

            <?php endforeach; ?>
        
        <?php endif;?>
 
      
        
            
            <?php // news section
			if($recent_posts): ?>
            
            	 <div class="section_block">
                
                 <h4>News</h4>
                    
                        <?php foreach($recent_posts as $post): setup_postdata($post); ?>
                        	<div class="news_item">
                                <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><?php the_title(); ?></a>
                                <span class="news_excerpt"><?php the_excerpt(); ?></span>
                                <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">Read More</a>
                            </div><!--.news_item--> 
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        
                        
                        
                        
                  <?php lifestream(); ?>
                  
                  <ul>
						<?php $events = $lifestream->get_events(array('number_of_results' => 4));
                        
                        foreach ($events as $event) {
                            echo '<li>'.$event->render().'</li>';
                        } ?>
                  </ul>
                        
            </div><!--.section_block--> 
            
            <?php endif; ?>
            
            
       </div><!--.section_buttons--> 

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
