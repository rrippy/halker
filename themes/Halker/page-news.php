<?php get_header(); the_post(); 

$recent_posts = get_posts(array(
	'post_type' => 'post',
	'numberposts' => 20,
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'post_date'
));

?>

    <div id="main" class="clearfix">
    
    	<header class="main_title">
        
            <h4>News</h4>
        
        </header>
    
    
    <div class="section_main_content clearfix">
    
    
     <?php // news section
			if($recent_posts): ?>
                    
					<?php foreach($recent_posts as $post): setup_postdata($post); ?>
                        <article>
                            <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><h5><?php the_title(); ?></h5></a>
                            <?php the_content(); ?>
                       </article>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
            
            <?php endif; ?>
     
    
    <div id="secondary">
        <?php get_sidebar('blog'); ?>
    </div><!-- #secondary -->
        
        
        
        </div><!-- .section_main_content -->
        
        
    </div><!-- #main -->    



<?php get_footer(); ?>