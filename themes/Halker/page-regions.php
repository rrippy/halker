<?php get_header(); the_post(); 
$intro_image = get_field('intro_image');
$intro_text = get_field('intro_text');
$sidebar_text = get_field('sidebar_text');
$sidebar_images = get_field('sidebar_images');


?>







	
    <div id="main" class="clearfix">
       


        <!-- Narrower column if there is a sidebar -->
         <?php if($sidebar_text || $sidebar_images): ?> 
         	<div class="three_quarter_columns">
          <?php endif; ?>
            
        		<h4><?php the_title(); ?></h4>
                
                 <?php if ($intro_image) { ?>
                	 <?php if($sidebar_text || $sidebar_images) { ?> 
						<?php $lead_image = wp_get_attachment_image_src($intro_image, 'three_quarter'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                     <?php } else { ?>
                     	<?php $lead_image = wp_get_attachment_image_src($intro_image, 'slide-image'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                      <?php } ?>
            	<?php } ?>
                
                <div class="region_map">
                	<img src="<?php bloginfo( 'template_url' );?>/images/map1.jpg" />
                    
                   <!-- REGION BUTTONS HERE --> 
                   <a class="map_location btn_1"></a>
                   <a class="map_location btn_2"></a>
                   <a class="map_location btn_3"></a>
                   <a class="map_location btn_4"></a>
                    
                </div><!-- .region_map -->
                
                <?php if ($intro_text) { ?>
                	<h2><?= $intro_text ?></h2>
            	<?php } ?>
    
    			<?php the_content(); ?>
                
                
                
                
                
                
              
                
                <?php
				$pages = get_pages(array(
				  'sort_order' => 'ASC',
				  'sort_column' => 'menu_order', //post_title
				  'hierarchical' => 1,
				  'child_of' => $post->ID,
				  'parent' => -1,
				  'offset' => 0,
				  'post_type' => 'page',
				  'post_status' => 'publish'
				));
				?>
				<?php if ($pages) { ?>
					<?php foreach ($pages as $page):
						$content = apply_filters('the_content', $page->post_content);
						$title = $page->post_title;
						$slug = $page->post_name;
						$postID = $page->ID; 
						$slider_images = get_field('slider_images', $postID);
						$intro_image = get_field('intro_image', $postID);
						$excerpt_text = get_field('intro_text', $postID);

					?>
                    
                  <!--hide link for williston-->
                  <?php if ($postID != 379) { ?> 
                  	<a href='<?php echo esc_url(home_url('/regions/'))?><?php echo "$slug"?>' class="<?php echo $postID ?>">
                  <?php } ?> 
                  
                  
                  <div class="map_hover_info <?php echo $slug; ?>">
         
					  
					  <?php if($slider_images): ?>
						 <div class="flexslider map">
          					<ul class="slides"> 
                            
                              <?php // if ($intro_image) { ?>
									<?php // $lead_image = wp_get_attachment_image_src($intro_image, 'map-image'); ?>
                                	<!--<li class="subpage_slide"><img src="<?php // echo $lead_image[0]; ?>" class="lead_img" /></li>-->
                              <?php //} ?>
                            
							  <?php foreach($slider_images as $slider_image): ?>
                              		<li id="<?php echo "$slug" ?>" class="subpage_slide">
                              
										<?php $slide_image = wp_get_attachment_image_src($slider_image['slider_image'], 'map-image'); ?>
                                        <img src="<?php echo $slide_image[0]; ?>" />
									
                                    </li>
							<?php endforeach; ?>
                             
						  </ul><!--.slides--> 
        			    </div><!--.flexslider.map-->
					 <?php endif; ?>
                    
					
                
            
            
             <div class="map_right">
            
            	<h6><?php echo $title; ?></h6>
            
				<?php if($excerpt_text): ?>
                    <div class="excerpt_text"><?=$excerpt_text ?></div>
                <?php endif; ?>
            
            
            	 <!--hide link for williston-->
				  <?php if ($postID != 379) { ?> 
                    <h6>Learn more ></h6> 
                  <?php } ?>
            	
                
             </div><!--.map_right-->
            
            
            
            </div><!--.map_hover_info-->
            
            <!--hide link for williston-->
		  <?php if ($postID != 379) { ?> 
            </a>
          <?php } ?>
                
                
         <?php endforeach; ?><!--.subpage_slides-->
	<?php } ?> 
    
    
         
                
                
                
                
          <?php if($sidebar_text || $sidebar_images): ?> 
         	</div><!-- .three_quarter_columns -->
          <?php endif; ?>
                
        
        
        
        <?php if($sidebar_text || $sidebar_images): ?> 
        
        <div class="project_sidebar quarter">
        
        	<?php if ($sidebar_text) { ?>
                <div class="sidebar_text"><?= $sidebar_text ?></div>
            <?php } ?>
        
        	<?php foreach($sidebar_images as $sidebar_image): ?>
	
    			<div class="sidebar_image">
                
                	<?php if($sidebar_image['sidebar_image_link']) { ?>
                  		<a href="<?= $sidebar_image['sidebar_image_link'] ?>">
                    <?php } elseif($sidebar_image['sidebar_pdf_link']) {  ?>
                    	<a href="<?= $sidebar_image['sidebar_pdf_link'] ?>" target="_blank">
                    <?php } ?>
                      
                      		<?php $sidebar_img = wp_get_attachment_image_src($sidebar_image['sidebar_image'], 'sidebar-thumb'); ?>
                      		<img src="<?php echo $sidebar_img[0]; ?>" />
                      
                  <?php if($sidebar_image['sidebar_image_link'] || ($sidebar_image['sidebar_pdf_link']) ): ?>
                  		</a> 
                    <?php endif; ?>
                  
               </div><!--.sidebar_image.quarter--> 

            <?php endforeach; ?>
            
            </div><!--.project_sidebar--> 
        
        <?php endif;?>
        
        
        
        

            
            
       

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
