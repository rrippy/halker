<?php
/**
 * Template Name: Solutions Page
 * Description: This is a template for the individual solutions pages
 */

?>

<?php get_header(); the_post(); 
$slider_images = get_field('slider_images');
$intro_image = get_field('intro_image');
$intro_text = get_field('intro_text');
$sidebar_text = get_field('sidebar_text');
$sidebar_images = get_field('sidebar_images');


?>







	
    <div id="main" class="clearfix">
    
    
    <nav class="service_nav sub-nav" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'solutions-menu' ) ); ?>
    </nav><!-- .service_nav.sub-nav -->
    
    

        <?php if($slider_images): ?>
        
        <div class="flexslider">
          <ul class="slides">
            <?php foreach($slider_images as $slider_image): ?>
                <li>
                  <?php $slide_image = wp_get_attachment_image_src($slider_image['slider_image'], 'slide-image'); ?>
                  <img src="<?php echo $slide_image[0]; ?>" />
                  <?php if($slider_image['image_caption']): ?>
                        <div class="value_overlay"><h3 class="flex-caption"><?=$slider_image['image_caption'] ?></h3></div>
                  <?php endif; ?>
                </li>
            <?php endforeach; ?>
          </ul><!--.slides--> 
        </div><!--.flexslider--> 
            
        
        <?php endif;?>
    	    
        
        
        
        <!-- Narrower column if there is a sidebar -->
         <?php if($sidebar_text || $sidebar_images) { ?> 
         	<div class="three_quarter_columns">
          <?php } else { ?>
          	<div class="full_culumn">
          <?php } ?>
            
        		<h4><?php the_title(); ?></h4>
                
                 <?php if ($intro_image) { ?>
                	 <?php if($sidebar_text || $sidebar_images) { ?> 
						<?php $lead_image = wp_get_attachment_image_src($intro_image, 'three_quarter'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                     <?php } else { ?>
                     	<?php $lead_image = wp_get_attachment_image_src($intro_image, 'slide-image'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                      <?php } ?>
            	<?php } ?>
                
                <?php if ($intro_text) { ?>
                	<h2><?= $intro_text ?></h2>
            	<?php } ?>
    
    			<?php the_content(); ?>
                
         	</div><!-- .three_quarter_columns or .full_culumn -->
                
        
        
        
        <?php if($sidebar_text || $sidebar_images): ?> 
        
        <div class="project_sidebar quarter">
        
        	<?php if ($sidebar_text) { ?>
                <div class="sidebar_text"><?= $sidebar_text ?></div>
            <?php } ?>
        
        	<?php foreach($sidebar_images as $sidebar_image): ?>
	
    			<div class="sidebar_image">
                
                	<?php if($sidebar_image['sidebar_image_link']) { ?>
                  		<a href="<?= $sidebar_image['sidebar_image_link'] ?>" class="page_link">
                    <?php } elseif($sidebar_image['sidebar_pdf_link']) {  ?>
                    	<a href="<?= $sidebar_image['sidebar_pdf_link'] ?>" target="_blank" class="pdf_link">
                    <?php } ?>
                      
                      		<?php $sidebar_img = wp_get_attachment_image_src($sidebar_image['sidebar_image'], 'sidebar-thumb'); ?>
                      		<img src="<?php echo $sidebar_img[0]; ?>" />
                      
                  <?php if($sidebar_image['sidebar_image_link'] || ($sidebar_image['sidebar_pdf_link']) ): ?>
                  		</a> 
                    <?php endif; ?>
                  
               </div><!--.sidebar_image.quarter--> 

            <?php endforeach; ?>
            
            </div><!--.project_sidebar--> 
        
        <?php endif;?>
        
        
        
        

            
            
       

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
