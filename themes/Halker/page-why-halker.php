<?php get_header(); the_post(); 
$slider_images = get_field('slider_images');
$intro_image = get_field('intro_image');
$intro_text = get_field('intro_text');


?>







	
    <div id="main" class="clearfix">
    
   


        <?php if($slider_images): ?>
        
        <div class="flexslider">
          <ul class="slides">
            <?php foreach($slider_images as $slider_image): ?>
                <li>
                  <?php $slide_image = wp_get_attachment_image_src($slider_image['slider_image'], 'slide-image'); ?>
                  <img src="<?php echo $slide_image[0]; ?>" />
                  <?php if($slider_image['image_caption']): ?>
                        <div class="value_overlay"><h3 class="flex-caption"><?=$slider_image['image_caption'] ?></h3></div>
                  <?php endif; ?>
                </li>
            <?php endforeach; ?>
          </ul><!--.slides--> 
        </div><!--.flexslider--> 
            
        
        <?php endif;?>
    	    
        
        
        
            
        		<h4><?php the_title(); ?></h4>
                
                 <?php if ($intro_image) { ?>
                	 <?php if($sidebar_text || $sidebar_images) { ?> 
						<?php $lead_image = wp_get_attachment_image_src($intro_image, 'three_quarter'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                     <?php } else { ?>
                     	<?php $lead_image = wp_get_attachment_image_src($intro_image, 'slide-image'); ?>
                        <img src="<?php echo $lead_image[0]; ?>" class="lead_img" />
                      <?php } ?>
            	<?php } ?>
                
                <?php if ($intro_text) { ?>
                	<h2><?= $intro_text ?></h2>
            	<?php } ?>
    
    			<?php the_content(); ?>
                
                
        
                 
            
       

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
