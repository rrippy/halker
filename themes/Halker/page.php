<?php
get_header(); the_post(); 
//$content_blocks = get_field('content_blocks');
//$intro_quote = get_field('intro_quote');

?>
	
    <div id="main" class="clearfix">
    
    	<header class="main_title">
        
            <h4><?php the_title(); ?></h4>
            
            <?php if ($intro_quote) { ?>
                <div class="intro_text"><?= $intro_quote ?></div>
            <?php } ?>
        
        </header>
        
        
        <?php if(get_the_content()) { ?>
			<div class="section_main_content">
				<?php the_content();?>
			</div>      
        <?php } ?>
        
        

        <?php if($content_blocks): ?>
        
        	<div class="section_block_content"> 
          
			<?php foreach($content_blocks as $content_block): ?>
              
              <div class="section_block <?= $content_block['block_slug'] ?>">
                 
                <?php if($content_block['block_title']): ?>
                    <h4><?= $content_block['block_title'] ?></h4>
                <?php endif; ?>
                
                <?php if($content_block['block_content']): ?>
                    <?= $content_block['block_content'] ?>
                <?php endif; ?>
                
				<?php if($content_block['block_link']): ?>
                    <div class="learn_btn"><a href="<?= $content_block['block_link'] ?>"><div class="learn_icon"></div>learn more</a></div>
                <?php endif; ?>
                
                <?php if($content_block['block_attachment']): ?>
                    <div class="download_btn"><a href="<?= $content_block['block_attachment'] ?>"><div class="download_icon">download</a></div>
                <?php endif; ?>

              
              </div><!--.section_block-->  
                    
            <?php endforeach; ?>
            
            </div><!--.section_block_content--> 

        <?php endif;?>
    	   

       
        
        
    </div><!-- #main -->
   


<?php get_footer(); ?>
	
   
	
