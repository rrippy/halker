<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 * Page template with a fixed 940px container and right sidebar layout
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>

	<div id="main" class="clearfix">
    
   		<header class="main_title">
        
            <h4>News</h4>
        
        </header>
        
        
   
   <div class="section_main_content">
		
		<div id="primary">
		<?php while ( have_posts() ) : the_post(); ?>
  
        	 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                
                <div class="news_content">
                
                	<header>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
                                <h5><?php the_title();?></h5>
                            </a>
                             <div class="news_date"><?php the_date();?></div>
                    </header>
             
                	<?php the_content();?>
                
                </div><!--.news_content-->
              
           </article>
        
        <?php endwhile; // End the loop ?>
    	
        <?php comments_template(); ?>
        
        <?php bootstrapwp_content_nav('nav-below');?>
        
        </div><!-- #primary -->
    
    <div id="secondary">
        <?php get_sidebar('blog'); ?>
    </div><!-- #secondary -->

        
        
    </div><!-- .section_main_content -->
    
    
    </div><!-- #main -->



<?php get_footer(); ?>